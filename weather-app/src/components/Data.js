import React , {Component} from 'react';
import WeatherCard from './WeaherCard';

class Dropdown extends Component{

  constructor(props){
    super(props)

    this.state = {
        data : {}
    };

    this.handlerClick = this.handlerClick.bind(this)
  }

  handlerClick(e) {
    const API_KEY = '921e98642c658ca33fc4c99b71c00045';

    const event = e.target ;
    const latitude = event.getAttribute("lat");
    const longitude = event.getAttribute("lon");
    console.log("latitude... ",latitude)
    console.log("longitude... ",longitude)

    fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${API_KEY}`)
    .then((res) => res.json())
    .then((json) => {
        console.log(json);

        document.getElementsByClassName("hiii").innerHtml ="";

        // <WeatherCard  />
        this.setState((p) => ({
        ...p,
        data: json, 
        }));
    });

  }

  render(){
    return (
      <div className="hiii">
          <p onClick={this.handlerClick} lat={this.props.data.lat} lon={this.props.data.lon} key={this.props.data.name}>{this.props.data.name} {this.props.data.state ?  ',' : ''} {this.props.data.state ?  this.props.data.state : ''} , {this.props.data.country}</p>
      </div>
      
    );
  }

  

}

// const Dropdown = (props) => {  
  
//   return (
//     <div >
//         <p key={props.data.name}>{props.data.name} {props.data.state ?  ',' : ''} {props.data.state ?  props.data.state : ''} , {props.data.country}</p>
//     </div>
    
//   );
// };

export default Dropdown;