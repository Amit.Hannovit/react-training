import React , {Component} from 'react';


const WeatherCard = (props) => {  
  
  return (
    <div >
        <p >City : {props.data.name} </p>
        <p >Temperature : {props.data.main.temp} </p>
        <p >Humidity : {props.data.main.humidity} </p>
        <p >Weather : {props.data.weather[0].description} </p>
    </div>
    
  );
};

export default WeatherCard;