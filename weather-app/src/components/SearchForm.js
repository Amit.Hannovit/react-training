import React, { Component } from 'react';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import WeatherCard from './WeaherCard';
import Container from '@mui/material/Container';

const API_KEY = '921e98642c658ca33fc4c99b71c00045';

class SearchBox extends Component {
	constructor(props) {
		super(props);

		this.state = {
			data: [],
			customizedData: [],
			city: '',
			latitude: '',
			longitude: '',
			getCityDataLoded: false,
			cityRelatedData: {},
		};

		this.handlerChange = this.handlerChange.bind(this);
	}

	handlerChange(e) {
		console.log('val.. ', e.target.value);
		this.setState((p) => ({
			...p,
			city : e.target.value,
		}));

		fetch(
			`http://api.openweathermap.org/geo/1.0/direct?q=${e.target.value}&limit=5&appid=${API_KEY}`
		)
			.then((res) => res.json())
			.then((json) => {
				console.log(json);			

				this.setState((p) => ({
					...p,
					city : e.target.value,
					data: json.cod === '400' || !e.target.value ? [] : json,
					customizedData:
						json.cod === '400' || !e.target.value
							? []
							: this.state.customizedData,
					getCityDataLoded: false,
				}));
			});
	}

	renderSuggestions = () => {
		if (this.state.data.length === 0) {
			return null;
		}
		return (
			<ul>
				{this.state.data.map((item) => (
					<li
						key={item.lat}
						data={item}
						onClick={(e) => this.handlerGetData(item)}
					>
						{item.name} {item.state ? ', ' : ''} {item.state ? item.state : ''}{' '}
						, {item.country}
					</li>
				))}
			</ul>
		);
	};

	handlerGetData(e) {
		console.log('>>>>>', e.lat);
		this.setState((p) => ({
			...p,
			city: e.name,
			latitude: e.lat,
			longitude: e.lon,
		}));

		fetch(
			`https://api.openweathermap.org/data/2.5/weather?lat=${e.lat}&lon=${e.lon}&appid=${API_KEY}`
		)
			.then((res) => res.json())
			.then((json) => {
				console.log(json);

				<p>City : {json.city} </p>;
				this.setState((p) => ({
					...p,
					getCityDataLoded: true,
					cityRelatedData: json,
				}));
			});
	}

	render() {
		return (
			<div>
				{/* <Container component='main' maxWidth='xs'> */}
				<h1>Weather App</h1>
				{/* <input onChange={this.handlerChange}  value={this.state.getCityDataLoded ? this.state.city : this.value} /> */}
				<input onChange={this.handlerChange} value={this.state.city}  />
				{/* {this.renderSuggestions()} */}
				{this.state.getCityDataLoded ? (
					<WeatherCard data={this.state.cityRelatedData} />
				) : (
					this.renderSuggestions()
				)}
				{/* <Autocomplete
						disablePortal
						id='combo-box-demo'
						options={this.state.customizedData}
						sx={{ width: 300, alignItems: 'center' }}
						onChange={(event, selectedValue) =>
							this.handlerGetData(selectedValue)
						}
						renderInput={(params) => (
							<TextField
								{...params}
								label='City'
								onChange={this.handlerChange}
							/>
						)}
					/>
				</Container>

				{this.state.getCityDataLoded ? (
					<WeatherCard data={this.state.cityRelatedData} />
				) : (
					''
				)} */}
			</div>
		);
	}
}

export default SearchBox;
