import logo from './logo.svg';
import './App.css';
import SearchBar from './components/SearchForm';

function App() {
  return (
    <div className="App">
      <header className="">
        <SearchBar />
      </header>
    </div>
  );
}

export default App;
