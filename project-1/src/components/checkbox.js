import React from 'react';

// DAY 4  task

const Checbox = (props) => {  
  
  return (
    <div className='row'>
        <div className="form-check">
            <input className="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked={props.checked} />
            <label className="form-check-label" for="flexCheckChecked">
                {props.name}
            </label>
        </div>
    </div>
    
    
  );
};

export default Checbox;