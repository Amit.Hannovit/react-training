import React from 'react';


// DAY 4  task

const SwapiCard = (props) => {  
  
  return (
    <div className="card text-black col-sm-6" >
        <div className="card-header"></div>
        <div className="card-body">
            <h5 className="card-title">Name : {props.value.name}</h5>
            <p className="card-text">birth_year : {props.value.birth_year}</p>
            <p className="card-text">eye_color : {props.value.eye_color}</p>
            <p className="card-text">gender :{props.value.gender}</p>
            <p className="card-text">hair_color : {props.value.hair_color}</p>
            <p className="card-text">height : {props.value.height}</p>
            <p className="card-text">mass : {props.value.mass}</p>
            <p className="card-text">skin_color : {props.value.skin_color}</p>           
            
            <p className="card-text">species : {props.value.species}</p>
            <p className="card-text">starships :{props.value.starships}</p>
            
            
        </div>
    </div>
    
  );
};

export default SwapiCard;