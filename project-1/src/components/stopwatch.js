import React , {Component} from "react"

class Conter extends Component{

    constructor(props){
        super(props)

        this.state = {
            time : 0,
            is_start : false,
            is_pause : true
        }

    }

    startTimer() {
        this.setState({
            is_pause : false,
            is_start : true
        });

        if (this.timerID) {
            clearInterval(this.timerID);
        }
      

        this.timerID = setInterval(
            () => {
                this.setState({
                    time: this.state.time + 10
                });
            },
            10
        );
        
        
        
        
        
    }
    

    stopTimer() {
        this.setState({
            is_pause: true,
            is_start : false
        },() => {
            clearInterval(this.timerID);
        });
        
        console.log("pause ...  ",this.state.is_pause)
    }

    resetTimer() {
        this.setState({
            time: 0,
            is_start : false,
            
        },() => {
            clearInterval(this.timerID);
        });
        
    }

    

    render() {
        return (
            <div>
                <h1>Stopwatch Task</h1>
                <div className="">
                    <span className="">
                        {("0" + Math.floor((this.state.time / 60000) % 60)).slice(-2)}:
                    </span>
                    <span className="">
                        {("0" + Math.floor((this.state.time / 1000) % 60)).slice(-2)}.
                    </span>
                    <span className="">
                        {("0" + ((this.state.time / 10) % 100)).slice(-2)}
                    </span>
                </div>
                {this.state.is_start ? 
                <div>
                    <button className="bg-danger" onClick={() => this.stopTimer()}>Stop</button> 
                    <button className="" onClick={() => this.resetTimer()}>Reset</button>
                </div>
                : 
                <div>
                    <button className="bg-success" onClick={() => this.startTimer()}>Start</button>
                    {this.state.is_pause ? <button className="" onClick={() => this.resetTimer()}>Reset</button> : ""}
                </div>
                
                }
                
                {/* <button className="bg-success" onClick={() => this.startTimer()}>Start</button>
                <button className="bg-danger" onClick={() => this.stopTimer()}>Stop</button>
                <button className="" onClick={() => this.resetTimer()}>Reset</button> */}
            </div>
        
        )

    }
}

export default Conter