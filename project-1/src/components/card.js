import React from 'react';


// DAY 4  task

const Card = (props) => {  
  
  return (
    <div className={`card text-black ${props.cssClass} col-sm-3`} >
        <div className="card-header"><img src={props.image} width={70} /></div>
        <div className="card-body">
            <h5 className="card-title">{props.title}</h5>
            <p className="card-text">{props.description}</p>
            <a href="#" className="btn btn-primary">Read More</a>
        </div>
    </div>
    
  );
};

export default Card;