import React , {Component} from "react"

class Conter extends Component{

    constructor(props){
        super(props)

        this.state = {
            count : 0,
            maxcount : 10
        }

    }

    increment() {
        this.setState({
            count : this.state.count < this.state.maxcount ? this.state.count + 1 : 10
        })
    }

    decrement() {
        this.setState({
            count : this.state.count - 1
        })
    }

    render() {
        return (
            <div>
                <h1>Counter task</h1>
                <h3>Count : {this.state.count} </h3>
                <button className="bg-success" onClick={() => this.increment()}>Increment</button>
                <button className="bg-danger" onClick={() => this.decrement()}>Decrement</button>
            </div>
        
        )

    }
}

export default Conter