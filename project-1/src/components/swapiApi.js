import React , {Component} from "react"
import SwapiCard from './SwapiCard'

class SwapiApi extends Component{

    constructor(props){
        super(props)

        this.state = {
            data : [],
            isLoading : false,
        }

    }

    dataHandler = () => {
        this.setState((p) => ({
            ...p,
            isLoading: true,
        }));

        fetch("https://swapi.dev/api/people")
        .then((res) => res.json())
        .then((json) => {
            console.log(json["results"]);
            this.setState((p) => ({
            ...p,
            data: json["results"],
            isLoading: false,
            }));
        });
    }
    

    

    render() {
        const { data, isLoading } = this.state;
        if (isLoading) {
        return (
            <div>
            <h2 className="gred-back">Please Wait...</h2>
            </div>
        );
        } else if (data.length <= 0) {
        return (
            <div>
            <button
                variant="contained"
                className="sw_btn"
                onClick={() => this.dataHandler()}
            >
                Get Star Wars People
            </button>
            </div>
        );
        } else {

    
        return (
            <div>
                <h1>Data</h1>

                <div className="row">
                    {data.map((item, index) => (
                            <SwapiCard key={index} value={item} />
                    ))};
                </div>
                

                
                
            </div>
        
        );
        };

    }
}

export default SwapiApi