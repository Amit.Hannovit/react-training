import React, {Component} from 'react';
import Data from './SearchData.json'

// DAY 5  task

class SearchBox extends Component{

    

    constructor(props){
        super(props)

        this.state = {
            data : Data
        }

    } 
    

    inputHandler(e) {
        let inputText = e.target.value.trim().toLowerCase();
        let filterData = Data.filter(word => word.toLowerCase().startsWith(inputText))
        this.setState({
            data : filterData

        })
    }
  
    render() {
        return (
            <div className="col-sm-12 text-center" >
                <h1>Search Box</h1>
                <input className='form-group' type={'text'} onChange={(e) => this.inputHandler(e)} />
                <div>
                    <ul>
                    {this.state.data.map((item) => (
                        <li key={item}>{item}</li>
                    ))}
                    </ul>
                </div>
            </div>
            
        );
    }
};

export default SearchBox;