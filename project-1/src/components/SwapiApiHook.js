import React,{useEffect,useState} from 'react';
import axios from 'axios';
import SwapiCard from './SwapiCard'

function DataFetching(){
    const [data,setData] = useState([]);
    const [isLoading,setIsLoading] = useState(false);

    const dataHandler = () => {

        setIsLoading(true)

        // data fetching using axios method
        axios.get("https://swapi.dev/api/people").then((res => {
            // console.log("res..  ",res["data"]["results"])
            setData(res["data"]["results"]);
            setIsLoading(false)
        })).catch(err => {
            console.log(err)
        }) ;

        // 2nd method for dt fetching

        // fetch("https://swapi.dev/api/people")
        // .then((res) => res.json())
        // .then((json) => {
        //     console.log(json["results"]);
        //     setData(json["results"]);
        //     setIsLoading(false)
            
        // });
    }

    

    if (isLoading) {
        return (
            <div>
            <h2 className="gred-back">Please Wait...</h2>
            </div>
        );
        } else if (data.length <= 0) {
        return (
            <div>
            <button
                variant="contained"
                className="sw_btn"
                onClick={dataHandler}
            >
                Get Star Wars People
            </button>
            </div>
        );
        } else {

    
        return (
            <div>
                <h1>Data</h1>

                <div className="row">
                    {data.map((item, index) => (
                            <SwapiCard key={index} value={item} />
                    ))};
                </div>
                

                
                
            </div>
        
        );
        };
}

export default DataFetching