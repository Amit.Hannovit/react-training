import React from 'react';
import Alert from '@mui/material/Alert';


function FormError({formErrors}) {
  return (
    <div className='formErrors'>
    {Object.keys(formErrors).map((fieldName, i) => {
      if(formErrors[fieldName].length > 0){
        return (
          <Alert severity="error" key={i}>{fieldName === 'remember_me' ? "Checkbox" : fieldName} {formErrors[fieldName]}</Alert>
        )        
      } else {
        return '';
      }
    })}
  </div>
  );
}

export default FormError;


