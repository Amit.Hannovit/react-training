import React, { Component } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Card from '@mui/material/Box';
import FormError1 from './FormError';

const theme = createTheme({
	palette: {
		background: {
			default: '#e4f0e2',
		},
	},
});

class LoginForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: '',
			password: '',
			remember_me: false,
			formErrors: { email: '', password: '', remember_me: '' },
			emailValid: false,
			passwordValid: false,
			remember_meValid: false,
			formValid: false,
		};

		this.handleChange = this.handleChange.bind(this);
	}

	// function for input on change

	handleChange(e) {
		const name = e.target.name;
		const value =
			e.target.type === 'checkbox' ? e.target.checked : e.target.value;

		this.setState(
			{
				[name]: value,
			},
			() => {
				this.validateField(name, value);
			}
		);
	}

	// function for validate input field

	validateField(fieldName, value) {
		let fieldValidationErrors = this.state.formErrors;
		let emailValid = this.state.emailValid;
		let passwordValid = this.state.passwordValid;
		let remember_meValid = this.state.remember_meValid;

		switch (fieldName) {
			case 'email':
				emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
				fieldValidationErrors.email = emailValid ? '' : ' is invalid';
				break;
			case 'password':
				passwordValid = value.length >= 6;
				fieldValidationErrors.password = passwordValid ? '' : ' is too short';
				break;

			case 'remember_me':
				remember_meValid = value === true;
				fieldValidationErrors.remember_me = remember_meValid
					? ''
					: ' is not checked';
				break;
			default:
				break;
		}
		this.setState(
			{
				formErrors: fieldValidationErrors,
				emailValid: emailValid,
				passwordValid: passwordValid,
				remember_meValid: remember_meValid,
			},
			this.validateForm
		);
	}

	// form validation function

	validateForm() {
		this.setState({
			formValid:
				this.state.emailValid &&
				this.state.passwordValid &&
				this.state.remember_meValid,
		});
	}

	// function for click on submit button 
	handleSubmit(event) {
		event.preventDefult();
	}

	render() {
		return (
			<ThemeProvider theme={theme}>
				<Container component='main' maxWidth='xs'>
					<FormError1 formErrors={this.state.formErrors} />
					<CssBaseline />
					<Box
						sx={{
							marginTop: 8,
							display: 'flex',
							flexDirection: 'column',
							alignItems: 'center',
						}}
					>
						<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
							<LockOutlinedIcon />
						</Avatar>
						<Typography component='h1' variant='h5'>
							Sign in
						</Typography>

						<Box component='form' onSubmit={this.handleSubmit} sx={{ mt: 1 }}>
							<TextField
								margin='normal'
								required
								fullWidth
								id='email'
								label='Email Address'
								name='email'
								autoComplete='email'
								autoFocus
								onChange={this.handleChange}
							/>
							<TextField
								margin='normal'
								required
								fullWidth
								name='password'
								label='Password'
								type='password'
								id='password'
								autoComplete='current-password'
								onChange={this.handleChange}
							/>
							<FormControlLabel
								control={
									<Checkbox
										required
										checked={this.state.remember_me}
										type='checkbox'
										color='primary'
										name='remember_me'
										onChange={this.handleChange}
									/>
								}
								label='I agree to terms and conditions'
							/>
							<Button
								disabled={!this.state.formValid}
								type='submit'
								fullWidth
								variant='contained'
								sx={{ mt: 3, mb: 2 }}
							>
								Sign In
							</Button>
							<Grid container>
								<Grid item xs>
									<Link href='#' variant='body2'>
										Forgot password?
									</Link>
								</Grid>
							</Grid>
						</Box>
					</Box>
				</Container>
			</ThemeProvider>
		);
	}
}

export default LoginForm;
