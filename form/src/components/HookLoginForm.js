import React, { useState, useEffect } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Card from '@mui/material/Box';
import FormError1 from './FormError';

const theme = createTheme({
	palette: {
		background: {
			default: '#e4f0e2',
		},
	},
});

function LoginForm() {
	
	const [login_form, setLogin_form] = useState({
		email: '',
		password: '',
		remember_me: false,
		formErrors: { email: '', password: '', remember_me: '' },
		emailValid: false,
		passwordValid: false,
		remember_meValid: false,
		formValid: false,
	});

	// function for input on change

	function handleChange(e) {
		const name = e.target.name;
		const value =
			e.target.type === 'checkbox' ? e.target.checked : e.target.value;

        console.log("value  ..  ",name);

		setLogin_form({ ...login_form, name: value });

		validateField(name, value);

        console.log(login_form)
	}

	// function for validate input field

	function validateField(fieldName, value) {
		let fieldValidationErrors = login_form.formErrors;
		let emailValid = login_form.emailValid;
		let passwordValid = login_form.passwordValid;
		let remember_meValid = login_form.remember_meValid;

		switch (fieldName) {
			case 'email':
				emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
				fieldValidationErrors.email = emailValid ? '' : ' is invalid';
				break;
			case 'password':
				passwordValid = value.length >= 6;
				fieldValidationErrors.password = passwordValid ? '' : ' is too short';
				break;

			case 'remember_me':
				remember_meValid = value === true;
				fieldValidationErrors.remember_me = remember_meValid
					? ''
					: ' is not checked';
				break;
			default:
				break;
		}

		setLogin_form({
			...login_form,
			formErrors: fieldValidationErrors,
			emailValid: emailValid,
			passwordValid: passwordValid,
			remember_meValid: remember_meValid,
		});

        validateForm()
	}

	// form validation function

	function validateForm() {
		setLogin_form({
			...login_form,
			formValid:
				login_form.emailValid &&
				login_form.passwordValid &&
				login_form.remember_meValid
		});
	}

	// function for click on submit button
	function handleSubmit(event) {event.preventDefult();}

	return (
		<ThemeProvider theme={theme}>
			<Container component='main' maxWidth='xs'>
				<FormError1 formErrors={login_form.formErrors} />
				<CssBaseline />
				<Box
					sx={{
						marginTop: 8,
						display: 'flex',
						flexDirection: 'column',
						alignItems: 'center',
					}}
				>
					<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
						<LockOutlinedIcon />
					</Avatar>
					<Typography component='h1' variant='h5'>
						Sign in
					</Typography>

					<Box component='form' onSubmit={handleSubmit} sx={{ mt: 1 }}>
						<TextField
							margin='normal'
							required
							fullWidth
							id='email'
							label='Email Address'
							name='email'
							autoComplete='email'
							autoFocus
							onChange={(e) => handleChange(e)}
						/>
						<TextField
							margin='normal'
							required
							fullWidth
							name='password'
							label='Password'
							type='password'
							id='password'
							autoComplete='current-password'
							onChange={(e) => handleChange(e)}
						/>
						<FormControlLabel
							control={
								<Checkbox
									required
									checked={login_form.remember_me}
									type='checkbox'
									color='primary'
									name='remember_me'
									onChange={(e) => handleChange(e)}
								/>
							}
							label='I agree to terms and conditions'
						/>
						<Button
							disabled={!login_form.formValid}
							type='submit'
							fullWidth
							variant='contained'
							sx={{ mt: 3, mb: 2 }}
						>
							Sign In
						</Button>
						<Grid container>
							<Grid item xs>
								<Link href='#' variant='body2'>
									Forgot password?
								</Link>
							</Grid>
						</Grid>
					</Box>
				</Box>
			</Container>
		</ThemeProvider>
	);
}

export default LoginForm;
