/*  
    #question -4.	Create an arrow function that accepts an array and returns the unique elements in the array.
**/

let array = ["ab","1","2","1","ab","abc"];
let unique_array = array.filter((item, i, ar) => ar.indexOf(item) === i);
console.log(unique_array);

/** 
    ## 6. rite for..of loop to iterate through an array

 */

let city = ["Patna", "DElhi", "Mumbai", "2"];

for(let i = 0; i < city.length; i++) {
    console.log(city[i]);
}

/** 
    ## 7.	Write for..in loop to print an value of all properties in an object
 */

const object = { name:'amit',age:24,gender:'male' };

for (const property in object) {
    console.log(`${property}: ${object[property]}`);
}